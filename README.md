

Command line options

If you have an existing project

    Step 1: Switch to your repository's directory
    cd /path/to/your/repo

    Step 2: Connect your existing repository to Bitbucket
    git remote add origin https://xbalaji@bitbucket.org/xbalaji/xbalaji.bitbucket.io.git 
	git push -u origin master


If you are starting from scratch

	git clone https://xbalaji@bitbucket.org/xbalaji/xbalaji.bitbucket.io.git 
	cd xbalaji.bitbucket.io 
	echo "# My project's README" >> README.md 
	git add README.md 
	git commit -m "Initial commit" 
	git push -u origin master


